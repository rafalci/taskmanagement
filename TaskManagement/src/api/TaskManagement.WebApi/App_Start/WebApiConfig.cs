﻿using Newtonsoft.Json.Serialization;
using System.Web.Http;
using TaskManagement.App_Start;
using TaskManagement.Filters;
using TaskManagement.SystemServices.Automapper;

namespace TaskManagement
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //Json formatter config
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            config.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;

            // Register filters
            config.Filters.Add(new HandleExceptionAttribute());
            config.Filters.Add(new ValidateModelAttribute());

            // Automapper registration
            AutomapperConfig.RegisterAutomapper();

            // Unity registration
            UnityConfig.RegisterUnityContainer();
        }
    }
}
