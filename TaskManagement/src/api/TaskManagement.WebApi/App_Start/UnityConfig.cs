﻿using System.Web.Http;
using TaskManagement.Services.Repositories.Implementations;
using TaskManagement.Services.Repositories.Interfaces;
using TaskManagement.Services.Services.Implementations;
using TaskManagement.Services.Services.Interfaces;
using Unity;
using Unity.WebApi;

namespace TaskManagement.App_Start
{
    public static class UnityConfig
    {
        public static void RegisterUnityContainer()
        {
            var container = new UnityContainer();

            // Register repositories
            container.RegisterType<IDictionaryRepository, DictionaryRepository>();
            container.RegisterType<IEventLoggerRepository, EventLoggerRepository>();
            container.RegisterType<ITaskRepository, TaskRepository>();

            // Register services
            container.RegisterType<IDictionaryService, DictionaryService>();
            container.RegisterType<IEventLoggerService, EventLoggerService>();
            container.RegisterType<ITaskService, TaskService>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}