﻿using System.Threading.Tasks;
using System.Web.Http;
using TaskManagement.Services.Services.Interfaces;

namespace TaskManagement.Controllers
{
    [RoutePrefix("dictionary")]
    public class DictionaryController: BaseController
    {
        private readonly IDictionaryService _dictionaryService;

        public DictionaryController(IDictionaryService dictionaryService, 
            IEventLoggerService eventLogger): base(eventLogger)
        {
            _dictionaryService = dictionaryService;
        }

        /// <summary>Gets the task statuses.</summary>
        /// <returns>Dictionary of statuses</returns>
        [Route("task-statuses")]
        [HttpGet]
        public async Task<IHttpActionResult> GetTaskStatuses()
        {
            var result = await _dictionaryService.GetTaskStatuses();

            return Ok(result);
        }


        /// <summary>Gets the users.</summary>
        /// <param name="searchKeyword">The search keyword.</param>
        /// <returns></returns>
        [Route("users/{searchKeyword}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetUsers(string searchKeyword)
        {
            var result = await _dictionaryService.GetUsers(searchKeyword);

            return Ok(result);
        }
    }
}