﻿using System.Threading.Tasks;
using System.Web.Http;
using TaskManagement.Models.Common;
using TaskManagement.Models.Dto;
using TaskManagement.Models.Filters;
using TaskManagement.Models.ViewModels;
using TaskManagement.Services.Services.Interfaces;
using TaskManagement.SystemServices.Automapper.Wrapper;

namespace TaskManagement.Controllers
{
    [RoutePrefix("task-management")]
    public class TaskController : BaseController
    {
        public readonly ITaskService _taskService;

        public TaskController(IEventLoggerService eventLogger,
            ITaskService taskService)
            : base(eventLogger)
        {
            _taskService = taskService;
        }

        /// <summary>Gets the task groups.</summary>
        /// <param name="model">The model.</param>
        /// <returns>GridResuestModel with TaskGroup collection</returns>
        [HttpGet]
        [Route("task-group")]
        public async Task<IHttpActionResult> GetTaskGroups([FromUri]GridRequestModel model)
        {
            var taskGroups = await _taskService.GetTaskGroups(model);
            var result = MapperWrapper.Mapper.Map<GridResultModel<TaskGroupViewModel>>(taskGroups);

            return Ok(result);
        }

        /// <summary>Gets the task group by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("task-group/{id}")]
        public async Task<IHttpActionResult> GetTaskGroupById(int id)
        {
            var taskGroup = await _taskService.GetTaskGroupById(id);
            var result = MapperWrapper.Mapper.Map<TaskGroupViewModel>(taskGroup);

            return Ok(result);
        }

        /// <summary>Creates the task group.</summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("task-group")]
        public IHttpActionResult CreateTaskGroup([FromBody]TaskGroupViewModel model)
        {
            var taskGroupId = _taskService.CreateTaskGroup(MapperWrapper.Mapper.Map<TaskGroupDto>(model));

            return Ok(taskGroupId);
        }

        /// <summary>Updates the task group.</summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPut]
        [Route("task-group")]
        public IHttpActionResult UpdateTaskGroup([FromBody]TaskGroupViewModel model)
        {
            _taskService.UpdateTaskGroup(MapperWrapper.Mapper.Map<TaskGroupDto>(model));

            return Ok();
        }

        /// <summary>Removes the task group by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("task-group/{id}")]
        public IHttpActionResult RemoveTaskGroupById([FromUri]int id)
        {
            _taskService.RemoveTaskGroup(id);

            return Ok();
        }

        /// <summary>Gets the user tasks.</summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("user-task")]
        public async Task<IHttpActionResult> GetUserTasks([FromUri]GridRequestModel model, [FromUri] UserTaskFilter filters)
        {
            var userTask = await _taskService.GetUserTasks(model, filters);
            var result = MapperWrapper.Mapper.Map<GridResultModel<UserTaskViewModel>>(userTask);

            return Ok(result);
        }

        /// <summary>Gets the user task by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("user-task/{id}")]
        public async Task<IHttpActionResult> GetUserTaskById(int id)
        {
            var userTask = await _taskService.GetUserTaskById(id);
            var result = MapperWrapper.Mapper.Map<UserTaskViewModel>(userTask);

            return Ok(result);
        }

        /// <summary>Creates the user task.</summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("user-task")]
        public IHttpActionResult CreateUserTask([FromBody]UserTaskViewModel model)
        {
            var userTaskId = _taskService.CreateUserTask(MapperWrapper.Mapper.Map<UserTaskDto>(model));

            return Ok(userTaskId);
        }

        /// <summary>Updates the task group.</summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPut]
        [Route("user-task")]
        public IHttpActionResult UpdateTaskGroup([FromBody]UserTaskViewModel model)
        {
            _taskService.UpdateUserTask(MapperWrapper.Mapper.Map<UserTaskDto>(model));

            return Ok();
        }

        /// <summary>Removes the user task by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("user-task/{id}")]
        public IHttpActionResult RemoveUserTaskById([FromUri]int id)
        {
            _taskService.RemoveUserTask(id);

            return Ok();
        }
    }
}
