﻿using System.Web.Http;
using TaskManagement.Filters;
using TaskManagement.Services.Services.Interfaces;

namespace TaskManagement.Controllers
{
    [HandleException]
    [ValidateModel]
    public class BaseController: ApiController
    {
        public BaseController(IEventLoggerService eventLogger)
        {
            EventLogger = eventLogger;
        }

        public IEventLoggerService EventLogger { get; }

    }
}