﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using TaskManagement.Services.Repositories.Implementations;
using TaskManagement.Services.Services.Implementations;
using TaskManagement.Services.Services.Interfaces;

namespace TaskManagement.Filters
{
    public class HandleExceptionAttribute: ExceptionFilterAttribute
    {
        private readonly IEventLoggerService _eventLogger;

        public HandleExceptionAttribute()
        {
            _eventLogger = new EventLoggerService(new EventLoggerRepository());
        }

        public override Task OnExceptionAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            actionExecutedContext.Response = actionExecutedContext.Request
                .CreateErrorResponse(HttpStatusCode.InternalServerError, GetResponseMessage(actionExecutedContext));

            _eventLogger.LogError(actionExecutedContext.Exception);

            return base.OnExceptionAsync(actionExecutedContext, cancellationToken);
        }

        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            actionExecutedContext.Response = actionExecutedContext.Request
                .CreateErrorResponse(HttpStatusCode.InternalServerError, GetResponseMessage(actionExecutedContext));

            _eventLogger.LogError(actionExecutedContext.Exception);

            base.OnException(actionExecutedContext);
        }

        private string GetResponseMessage(HttpActionExecutedContext actionExecutedContext)
        {
            return $"500 - {actionExecutedContext.Exception.GetType()} - Message: {actionExecutedContext.Exception.Message}";
        }
    }
}