﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using TaskManagement.Models.Common;

namespace TaskManagement.Utils.Helpers
{
    public static class GridHelper
    {
        /// <summary>Paginates the data.</summary>
        /// <param name="model">The model.</param>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        public static IQueryable<T> PaginateData<T>(GridRequestModel model, IQueryable<T> data)
        {
            return data.Skip(model.Start)
                .Take(model.Length);
        }


        /// <summary>Sorts the data.</summary>
        /// <param name="model">The model.</param>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        public static IQueryable<T> SortData<T>(GridRequestModel model, IQueryable<T> data, IDictionary<string, string> columnMapping)
        {
            string sortBy = model.Columns[0].Data;
            string sortDir = "asc";

            if (model.Order != null)
            {
                var columnName = model.Columns[model.Order[0].Column].Data;

                sortBy = columnMapping.Keys.Contains(columnName)
                    ? columnMapping[columnName]
                    : columnName;
                sortDir = model.Order[0].Dir.ToLower();
            }

            return data.OrderBy($"{sortBy} {sortDir}");
        }
    }
}
