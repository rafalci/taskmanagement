﻿using AutoMapper;
using System;

namespace TaskManagement.SystemServices.Automapper.Wrapper
{
    public static class MapperWrapper
    {
        private static IConfigurationProvider _configuration;
        private static IMapper _mapper;

        private static IConfigurationProvider Configuration
        {
            get
            {
                if(_configuration == null)
                {
                    throw new Exception("Automapper is not initialize.");
                }
                return _configuration;
            }
            set
            {
                _configuration = value;
            }
        }

        public static IMapper Mapper
        {
            get
            {
                if (_mapper == null)
                {
                    throw new Exception("Automapper is not initialize.");
                }
                return _mapper;
            }
            set
            {
                _mapper = value;
            }
        }

        public static void Initialize(MapperConfiguration mapperConfiguration)
        {
            Configuration = mapperConfiguration;
            Mapper = Configuration.CreateMapper();
        }

        public static void AssertConfigurationIsValid()
        {
            Configuration.AssertConfigurationIsValid();
        }
    }
}
