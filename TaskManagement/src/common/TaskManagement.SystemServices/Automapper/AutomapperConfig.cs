﻿using AutoMapper;
using TaskManagement.SystemServices.Automapper.Profiles;
using TaskManagement.SystemServices.Automapper.Wrapper;

namespace TaskManagement.SystemServices.Automapper
{
    public static class AutomapperConfig
    {
        public static IMapper Mapper { get; private set; }

        public static void RegisterAutomapper()
        {
            MapperWrapper.Initialize(new MapperConfiguration(cfg =>
            {
                cfg.AllowNullCollections = true;

                cfg.AddProfile<EntityToDtoProfile>();
                cfg.AddProfile<DtoToViewModel>();
                cfg.AddProfile<DtoToEntity>();
                cfg.AddProfile<ViewModelToDto>();
            }));

            MapperWrapper.AssertConfigurationIsValid();
        }
    }
}
