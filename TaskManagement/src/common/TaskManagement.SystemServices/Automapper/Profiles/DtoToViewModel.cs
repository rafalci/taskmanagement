﻿using AutoMapper;
using System.Linq;
using TaskManagement.Models.Common;
using TaskManagement.Models.Dto;
using TaskManagement.Models.ViewModels;

namespace TaskManagement.SystemServices.Automapper.Profiles
{
    public class DtoToViewModel : Profile
    {
        public DtoToViewModel()
        {
            CreateMap<TaskGroupDto, TaskGroupViewModel>()
                .ForMember(dest => dest.NumberOfTasks, opt => opt.MapFrom(src => src.UserTasks.Count()));

            CreateMap<UserTaskDto, UserTaskViewModel>();

            CreateMap(typeof(GridResultModel<>), typeof(GridResultModel<>));
        }
    }
}
