﻿using AutoMapper;
using TaskManagement.Models.Dto;
using TaskManagement.Models.ViewModels;

namespace TaskManagement.SystemServices.Automapper.Profiles
{
    public class ViewModelToDto: Profile
    {
        public ViewModelToDto()
        {
            CreateMap<TaskGroupViewModel, TaskGroupDto>()
                .ForMember(dest => dest.UserTasks, opt => opt.Ignore());

            CreateMap<UserTaskViewModel, UserTaskDto>();
        }
    }
}
