﻿using AutoMapper;
using TaskManagement.DataAccess;
using TaskManagement.Models.Dto;

namespace TaskManagement.SystemServices.Automapper.Profiles
{
    public class DtoToEntity: Profile
    {
        public DtoToEntity()
        {
            CreateMap<EventLogDto, EventLog>()
                .ForMember(dest => dest.tblEventType, opt => opt.Ignore());

            CreateMap<UserTaskDto, UserTask>()
                .ForMember(dest => dest.tblTaskGroup, opt => opt.Ignore())
                .ForMember(dest => dest.tblTaskStatus, opt => opt.Ignore())
                .ForMember(dest => dest.tblUser, opt => opt.Ignore());

            CreateMap<TaskGroupDto, TaskGroup>()
                .ForMember(dest => dest.tblUserTasks, opt => opt.Ignore());
        }
    }
}
