﻿using AutoMapper;
using TaskManagement.DataAccess;
using TaskManagement.Models.Dto;

namespace TaskManagement.SystemServices.Automapper.Profiles
{
    public class EntityToDtoProfile: Profile
    {
        public EntityToDtoProfile()
        {
            CreateMap<TaskGroup, TaskGroupDto>()
                .ForMember(dest => dest.UserTasks, opt => opt.MapFrom(src => src.tblUserTasks));

            CreateMap<UserTask, UserTaskDto>()
                .ForMember(dest => dest.TaskGroupName, opt => opt.MapFrom(src => src.tblTaskGroup.TaskGroupName))
                .ForMember(dest => dest.StatusName, opt => opt.MapFrom(src => src.tblTaskStatus.StatusName))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => $"{src.tblUser.FirstName} {src.tblUser.LastName}"));
        }
    }
}
