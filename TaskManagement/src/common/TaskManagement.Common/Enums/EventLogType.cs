﻿namespace TaskManagement.Common.Enums
{
    public enum EventLogType
    {
        Info = 1,
        Warning,
        Error
    }
}
