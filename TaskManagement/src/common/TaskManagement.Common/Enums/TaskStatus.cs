﻿namespace TaskManagement.Common.Enums
{
    public enum TaskStatus
    {
        New = 1,
        InProgress,
        Completed
    }
}
