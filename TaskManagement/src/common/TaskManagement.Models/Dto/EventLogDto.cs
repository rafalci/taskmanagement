﻿using System;
using TaskManagement.Common.Enums;

namespace TaskManagement.Models.Dto
{
    public class EventLogDto
    {
        public int EventLogId { get; set; }

        public EventLogType TypeId { get; set; }

        public string Message { get; set; }

        public DateTime LogTime { get; set; }
    }
}
