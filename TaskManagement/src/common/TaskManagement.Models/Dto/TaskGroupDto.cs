﻿using System.Collections.Generic;

namespace TaskManagement.Models.Dto
{
    public class TaskGroupDto
    {
        public int TaskGroupId { get; set; }

        public string TaskGroupName { get; set; }

        public IEnumerable<UserTaskDto> UserTasks { get; set; }
    }
}
