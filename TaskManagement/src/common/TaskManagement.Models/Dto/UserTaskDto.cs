﻿using System;

namespace TaskManagement.Models.Dto
{
    public class UserTaskDto
    {
        public string UserTaskName { get; set; }

        public int UserTaskId { get; set; }

        public DateTime Deadline { get; set; }

        public int? UserId { get; set; }

        public int StatusId { get; set; }

        public int TaskGroupId { get; set; }

        public string TaskGroupName { get; set; }

        public string StatusName  { get; set; }

        public string UserName { get; set; }
    }
}
