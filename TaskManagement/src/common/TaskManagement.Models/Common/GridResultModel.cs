﻿using System.Collections.Generic;
using System.Linq;

namespace TaskManagement.Models.Common
{
    public class GridResultModel<T>
    {
        public int RecordsTotal { get; set; }

        public int RecordsFiltered { get; set; }

        public IEnumerable<T> Data { get; set; }
    }
}
