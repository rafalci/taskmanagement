﻿using System.Collections.Generic;

namespace TaskManagement.Models.Common
{
    public class GridRequestModel
    {
        public int Draw { get; set; }

        public int Start { get; set; }

        public int Length { get; set; }

        public IList<Column> Columns { get; set; }

        public IList<Order> Order { get; set; }
    }

    public class Column
    {
        public string Data { get; set; }

        public string Name { get; set; }

        public bool Searchable { get; set; }

        public bool Orderable { get; set; }
    }

    public class Order
    {
        public int Column { get; set; }

        public string Dir { get; set; }
    }
}
