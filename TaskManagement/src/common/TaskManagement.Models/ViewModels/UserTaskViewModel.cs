﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace TaskManagement.Models.ViewModels
{
    public class UserTaskViewModel
    {
        [Required]
        [MaxLength(255)]
        public string UserTaskName { get; set; }

        public int UserTaskId { get; set; }

        [Required]
        public DateTime Deadline { get; set; }

        public string DeadlineText
        {
            get
            {
                return Deadline.ToString("MM/dd/yyyy HH:mm");
            }
        }


        [Required]
        public TaskStatus StatusId { get; set; }

        public string StatusName { get; set; }

        public int? UserId { get; set; }

        public string UserName { get; set; }

        [Required]
        public int TaskGroupId { get; set; }

        public string TaskGroupName { get; set; }
    }
}
