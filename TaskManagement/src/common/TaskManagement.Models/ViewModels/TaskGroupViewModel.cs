﻿using System.ComponentModel.DataAnnotations;

namespace TaskManagement.Models.ViewModels
{
    public class TaskGroupViewModel
    {
        public int TaskGroupId { get; set; }

        [Required]
        [MaxLength(255)]
        public string TaskGroupName { get; set; }

        public int NumberOfTasks { get; set; }
    }
}
