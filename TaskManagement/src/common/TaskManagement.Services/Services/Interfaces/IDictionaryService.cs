﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TaskManagement.Services.Services.Interfaces
{
    public interface IDictionaryService
    {
        /// <summary>Gets a dictrionary of the task statuses.</summary>
        /// <returns></returns>
        Task<IDictionary<int, string>> GetTaskStatuses();

        /// <summary>Gets the users.</summary>
        /// <param name="searchKeyword">The search keyword.</param>
        /// <returns></returns>
        Task<IDictionary<int, string>> GetUsers(string searchKeyword);
    }
}
