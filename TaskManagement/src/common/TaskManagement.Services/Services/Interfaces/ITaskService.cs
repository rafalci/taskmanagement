﻿using System.Threading.Tasks;
using TaskManagement.Models.Common;
using TaskManagement.Models.Dto;
using TaskManagement.Models.Filters;

namespace TaskManagement.Services.Services.Interfaces
{
    public interface ITaskService
    {

        /// <summary>Gets the task groups.</summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        Task<GridResultModel<TaskGroupDto>> GetTaskGroups(GridRequestModel model);

        /// <summary>Gets the user tasks.</summary>
        /// <param name="dataTable">The data table.</param>
        /// <returns></returns>
        Task<GridResultModel<UserTaskDto>> GetUserTasks(GridRequestModel dataTable, UserTaskFilter filter);

        /// <summary>Gets the task group by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task<TaskGroupDto> GetTaskGroupById(int id);

        /// <summary>Gets the user task by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task<UserTaskDto> GetUserTaskById(int id);

        /// <summary>Creates the task group.</summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        int CreateTaskGroup(TaskGroupDto model);

        /// <summary>Creates the user task.</summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        int CreateUserTask(UserTaskDto model);

        /// <summary>Updates the task group.</summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        void UpdateTaskGroup(TaskGroupDto model);

        /// <summary>Updates the user task.</summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        void UpdateUserTask(UserTaskDto model);

        /// <summary>Removes the task group.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        void RemoveTaskGroup(int id);

        /// <summary>Removes the user task.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        void RemoveUserTask(int id);
    }
}
