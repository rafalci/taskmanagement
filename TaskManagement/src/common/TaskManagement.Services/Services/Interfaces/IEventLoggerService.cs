﻿using System;
using System.Threading.Tasks;

namespace TaskManagement.Services.Services.Interfaces
{
    public interface IEventLoggerService
    {
        /// <summary>Logs the information.</summary>
        /// <param name="infoMessage">The information message.</param>
        void LogInfo(string infoMessage);

        /// <summary>Logs the warning.</summary>
        /// <param name="warningMessage">The warning message.</param>
        void LogWarning(string warningMessage);

        /// <summary>Logs the error.</summary>
        /// <param name="errorMessage">The error message.</param>
        void LogError(string errorMessage);

        /// <summary>Logs the error.</summary>
        /// <param name="exception">The exception.</param>
        void LogError(Exception exception);
    }
}
