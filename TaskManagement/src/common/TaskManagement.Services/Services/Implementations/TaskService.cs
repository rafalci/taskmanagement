﻿using System.Threading.Tasks;
using TaskManagement.DataAccess;
using TaskManagement.Models.Common;
using TaskManagement.Models.Dto;
using TaskManagement.Models.Filters;
using TaskManagement.Services.Repositories.Interfaces;
using TaskManagement.Services.Services.Interfaces;
using TaskManagement.SystemServices.Automapper.Wrapper;

namespace TaskManagement.Services.Services.Implementations
{
    public class TaskService: ITaskService
    {
        private readonly ITaskRepository _taskRepository;

        public TaskService(ITaskRepository taskRepository)
        {
            _taskRepository = taskRepository;
        }

        public int CreateTaskGroup(TaskGroupDto model)
        {
            return _taskRepository.CreateTaskGroup(MapperWrapper.Mapper.Map<TaskGroup>(model));
        }

        public int CreateUserTask(UserTaskDto model)
        {
            return _taskRepository.CreateUserTask(MapperWrapper.Mapper.Map<UserTask>(model));
        }

        public async Task<TaskGroupDto> GetTaskGroupById(int id)
        {
            return MapperWrapper.Mapper.Map<TaskGroupDto>(await _taskRepository.GetTaskGroupById(id));
        }

        public async Task<UserTaskDto> GetUserTaskById(int id)
        {
            return MapperWrapper.Mapper.Map<UserTaskDto>(await _taskRepository.GetUserTaskById(id));
        }

        public async Task<GridResultModel<TaskGroupDto>> GetTaskGroups(GridRequestModel model)
        {
            return MapperWrapper.Mapper.Map<GridResultModel<TaskGroupDto>>(await _taskRepository.GetTaskGroups(model));
        }

        public async Task<GridResultModel<UserTaskDto>> GetUserTasks(GridRequestModel dataTable, UserTaskFilter filter)
        {
            return MapperWrapper.Mapper.Map<GridResultModel<UserTaskDto>>(await _taskRepository.GetUserTasks(dataTable, filter));
        }

        public void RemoveTaskGroup(int id)
        {
            _taskRepository.RemoveTaskGroup(id);
        }

        public void RemoveUserTask(int id)
        {
            _taskRepository.RemoveUserTask(id);
        }

        public void UpdateTaskGroup(TaskGroupDto model)
        {
            _taskRepository.UpdateTaskGroup(MapperWrapper.Mapper.Map<TaskGroup>(model));
        }

        public void UpdateUserTask(UserTaskDto model)
        {
            _taskRepository.UpdateUserTask(MapperWrapper.Mapper.Map<UserTask>(model));
        }
    }
}
