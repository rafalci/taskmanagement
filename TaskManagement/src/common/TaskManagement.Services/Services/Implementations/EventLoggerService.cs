﻿using System;
using TaskManagement.Common.Enums;
using TaskManagement.DataAccess;
using TaskManagement.Models.Dto;
using TaskManagement.Services.Repositories.Interfaces;
using TaskManagement.Services.Services.Interfaces;
using TaskManagement.SystemServices.Automapper.Wrapper;

namespace TaskManagement.Services.Services.Implementations
{
    public class EventLoggerService : IEventLoggerService
    {
        private readonly IEventLoggerRepository _eventLoggerRepository;

        public EventLoggerService(IEventLoggerRepository eventLoggerRepository)
        {
            _eventLoggerRepository = eventLoggerRepository;
        }

        public void LogError(string errorMessage)
        {
            var log = GetEventLog(errorMessage, EventLogType.Error);

            _eventLoggerRepository.LogEvent(log);
        }

        public void LogError(Exception exception)
        {
            var log = GetEventLog(exception.ToString(), EventLogType.Error);

            _eventLoggerRepository.LogEvent(log);
        }

        public void LogInfo(string infoMessage)
        {
            var log = GetEventLog(infoMessage, EventLogType.Info);

            _eventLoggerRepository.LogEvent(log);
        }

        public void LogWarning(string warningMessage)
        {
            var log = GetEventLog(warningMessage, EventLogType.Warning);

            _eventLoggerRepository.LogEvent(log);
        }

        private EventLog GetEventLog(string message, EventLogType type)
        {
            var log = new EventLogDto
            {
                Message = message,
                TypeId = type,
                LogTime = DateTime.UtcNow
            };

            return MapperWrapper.Mapper.Map<EventLog>(log);
        }
    }
}
