﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TaskManagement.Services.Repositories.Interfaces;
using TaskManagement.Services.Services.Interfaces;

namespace TaskManagement.Services.Services.Implementations
{
    public class DictionaryService : IDictionaryService
    {
        private readonly IDictionaryRepository _dictionaryRepository;

        public DictionaryService(IDictionaryRepository dictionaryRepository)
        {
            _dictionaryRepository = dictionaryRepository;
        }

        public async Task<IDictionary<int, string>> GetTaskStatuses()
        {
            return await _dictionaryRepository.GetTaskStatuses();
        }

        public async Task<IDictionary<int, string>> GetUsers(string searchKeyword)
        {
            return await _dictionaryRepository.GetUsers(searchKeyword);
        }
    }
}
