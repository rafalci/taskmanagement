﻿using TaskManagement.DataAccess;

namespace TaskManagement.Services.Repositories
{
    public class GenericRepositoryFactory: GenericRepository<TaskManagementContext>
    {
        public GenericRepositoryFactory(): base() { }
    }
}
