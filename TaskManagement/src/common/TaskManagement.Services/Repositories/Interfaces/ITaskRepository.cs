﻿using System.Threading.Tasks;
using TaskManagement.DataAccess;
using TaskManagement.Models.Common;
using TaskManagement.Models.Filters;

namespace TaskManagement.Services.Repositories.Interfaces
{
    public interface ITaskRepository
    {
        /// <summary>Gets the task groups.</summary>
        /// <param name="dataTable">The data table.</param>
        /// <returns></returns>
        Task<GridResultModel<TaskGroup>> GetTaskGroups(GridRequestModel dataTable);

        /// <summary>Gets the user tasks.</summary>
        /// <param name="dataTable">The data table.</param>
        /// <returns></returns>
        Task<GridResultModel<UserTask>> GetUserTasks(GridRequestModel dataTable, UserTaskFilter filter);

        /// <summary>Gets the task group by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task<TaskGroup> GetTaskGroupById(int id);

        /// <summary>Gets the user task by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task<UserTask> GetUserTaskById(int id);

        /// <summary>Creates the task group.</summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        int CreateTaskGroup(TaskGroup model);

        /// <summary>Creates the user task.</summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        int CreateUserTask(UserTask model);

        /// <summary>Updates the task group.</summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        void UpdateTaskGroup(TaskGroup model);

        /// <summary>Updates the user task.</summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        void UpdateUserTask(UserTask model);

        /// <summary>Removes the task group.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        void RemoveTaskGroup(int id);

        /// <summary>Removes the user task.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        void RemoveUserTask(int id);
    }
}
