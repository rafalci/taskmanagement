﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TaskManagement.Services.Repositories.Interfaces
{
    public interface IDictionaryRepository
    {
        /// <summary>Gets a dictionary of the task statuses.</summary>
        /// <returns></returns>
        Task<IDictionary<int, string>> GetTaskStatuses();


        /// <summary>Gets the users.</summary>
        /// <param name="searchKeyword">The search keyword.</param>
        /// <returns></returns>
        Task<IDictionary<int, string>> GetUsers(string searchKeyword);
    }
}
