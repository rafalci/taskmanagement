﻿using System.Threading.Tasks;
using TaskManagement.DataAccess;

namespace TaskManagement.Services.Repositories.Interfaces
{
    public interface IEventLoggerRepository
    {
        /// <summary>Logs the event.</summary>
        /// <param name="log">The log.</param>
        void LogEvent(EventLog log);
    }
}
