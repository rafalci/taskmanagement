﻿using System.Threading.Tasks;
using TaskManagement.DataAccess;
using TaskManagement.Services.Repositories.Interfaces;

namespace TaskManagement.Services.Repositories.Implementations
{
    public class EventLoggerRepository : GenericRepositoryFactory, IEventLoggerRepository
    {
        public void LogEvent(EventLog log)
        {
            InsertSave(log);
        }
    }
}
