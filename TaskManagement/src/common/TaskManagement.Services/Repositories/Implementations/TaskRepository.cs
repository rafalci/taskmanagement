﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using TaskManagement.DataAccess;
using TaskManagement.Models.Common;
using TaskManagement.Models.Filters;
using TaskManagement.Services.Repositories.Interfaces;
using TaskManagement.Utils.Helpers;

namespace TaskManagement.Services.Repositories.Implementations
{
    public class TaskRepository : GenericRepositoryFactory, ITaskRepository
    {
        public int CreateTaskGroup(TaskGroup model)
        {
            InsertSave(model);
            return model.TaskGroupId;
        }

        public int CreateUserTask(UserTask model)
        {
            InsertSave(model);
            return model.UserTaskId;
        }

        public async Task<TaskGroup> GetTaskGroupById(int id)
        {
            return await Context.TaskGroups
                .FirstOrDefaultAsync(x => x.TaskGroupId == id);
        }

        public async Task<UserTask> GetUserTaskById(int id)
        {
            return await Context.UserTasks
                .FirstOrDefaultAsync(x => x.UserTaskId == id);
        }

        public async Task<GridResultModel<TaskGroup>> GetTaskGroups(GridRequestModel dataTable)
        {
            int recordsTotal = Context.TaskGroups.Count();

            var columnMapping = new Dictionary<string, string>
            {
                {"numberOfTasks", "tblUserTasks.Count" }
            };

            var data = GridHelper.SortData(dataTable, Context.TaskGroups, columnMapping);
            data = GridHelper.PaginateData(dataTable, data);

            return new GridResultModel<TaskGroup>
            {
                RecordsTotal = recordsTotal,
                RecordsFiltered = recordsTotal,
                Data = await data.ToListAsync()
            };
        }

        public async Task<GridResultModel<UserTask>> GetUserTasks(GridRequestModel dataTable, UserTaskFilter filter)
        {
            var filteredTasks = GetFilteredUserTasks(filter);

            int recordsTotal = filteredTasks.Count();

            var columnMapping = new Dictionary<string, string>
            {
                {"userName", "tblUser.FirstName" },
                {"statusName", "tblTaskStatus.StatusName" },
                {"deadlineText", "Deadline" },
            };

            var data = GridHelper.SortData(dataTable, filteredTasks, columnMapping);
            data = GridHelper.PaginateData(dataTable, data);

            return new GridResultModel<UserTask>
            {
                RecordsTotal = recordsTotal,
                RecordsFiltered = recordsTotal,
                Data = await data.ToListAsync()
            };
        }

        public void RemoveTaskGroup(int id)
        {
            var userTasks = Context.TaskGroups
                .FirstOrDefault(x => x.TaskGroupId == id);

            Context.UserTasks.RemoveRange(userTasks.tblUserTasks);

            RemoveSave<TaskGroup>(id);
        }

        public void RemoveUserTask(int id)
        {
            RemoveSave<UserTask>(id);
        }

        public void UpdateTaskGroup(TaskGroup model)
        {
            UpdateSave(model);
        }

        public void UpdateUserTask(UserTask model)
        {
            UpdateSave(model);
        }

        private IQueryable<UserTask> GetFilteredUserTasks(UserTaskFilter filter)
        {
            return filter.TaskGroupId != null
                ? Context.UserTasks.Where(x => x.TaskGroupId == filter.TaskGroupId)
                : Enumerable.Empty<UserTask>().AsQueryable();
        }
    }
}
