﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using TaskManagement.Services.Repositories.Interfaces;

namespace TaskManagement.Services.Repositories.Implementations
{
    public class DictionaryRepository : GenericRepositoryFactory, IDictionaryRepository
    {
        public async Task<IDictionary<int, string>> GetTaskStatuses()
        {
            return await Context.TaskStatuses
                .ToDictionaryAsync(key => key.StatusId, 
                    value => value.StatusName);
        }

        public async Task<IDictionary<int, string>> GetUsers(string searchKeyword)
        {
            return await Context.Users
                .Where(x => x.FirstName.ToLower().StartsWith(searchKeyword.ToLower())
                    || x.LastName.ToLower().StartsWith(searchKeyword.ToLower()))
                .ToDictionaryAsync(key => key.UserId,
                    value => $"{value.FirstName} {value.LastName}");
        }
    }
}
