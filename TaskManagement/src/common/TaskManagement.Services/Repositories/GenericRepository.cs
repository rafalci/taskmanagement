﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace TaskManagement.Services.Repositories
{
    public class GenericRepository<TContext> where TContext : DbContext, new()
    {
        public GenericRepository()
        {
            Context = new TContext();
        }

        protected TContext Context { get; }

        /// <summary>
        /// Save all current changes
        /// </summary>
        protected void SaveChanges()
        {
            Context.SaveChanges();
        }

        /// <summary>
        /// Insert data and save
        /// </summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <param name="model">Inserted data</param>
        protected void InsertSave<T>(T model) where T: class
        {
            Context.Set<T>().Add(model);
            SaveChanges();
        }

        /// <summary>Updates the save.</summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model">The model.</param>
        protected void UpdateSave<T>(T model) where T: class
        {
            Context.Entry(model).State = EntityState.Modified;
            SaveChanges();
        }

        /// <summary>Removes the save.</summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id">The identifier.</param>
        /// <exception cref="Exception">Entity with the following id does not exist.</exception>
        protected void RemoveSave<T>(int id) where T: class
        {
            var model = Context.Set<T>().Find(id);

            Context.Entry(model).State = EntityState.Deleted;
            SaveChanges();
        }
    }
}
