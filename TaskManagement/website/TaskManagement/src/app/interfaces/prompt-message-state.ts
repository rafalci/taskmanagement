import { PromptType } from '../enums/prompt-type';

export interface PromptMessageState {
    show: boolean;
    title: string;
    message: string;
    promptType: PromptType;
    yesCallback: () => void;
}
