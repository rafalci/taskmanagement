export interface DatatableFilterInterface {
    [param: string]: string | number | boolean;
}
