export class RouterLinkInterface {
    title: string;
    url: string;
    active: boolean;
}
