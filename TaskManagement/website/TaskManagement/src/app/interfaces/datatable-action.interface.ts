export interface DatatableActionInterface<T> {
    [actionName: string]: (item: T) => void;
}
