export enum PromptType {
    Info,
    Warning,
    Error,
    Question
}
