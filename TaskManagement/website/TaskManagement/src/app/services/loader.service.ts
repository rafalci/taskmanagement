import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class LoaderService {

    private loaderSubject = new Subject<boolean>();
    get loaderState() { return this.loaderSubject.asObservable(); }

    constructor() { }

    showLoader() {
        this.loaderSubject.next(true);
    }

    hideLoader() {
        this.loaderSubject.next(false);
    }
}
