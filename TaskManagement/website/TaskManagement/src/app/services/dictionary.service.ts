import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class DictionaryService {
    constructor(private http: HttpClient) {
    }

    /**
     * Gets task statuses
     * @returns An Observable of keyvalue array.
     */
    getTaskStatuses(): Observable<any[]> {
        const url = environment.apiUrl + '/dictionary/task-statuses?';
        return this.http.get<any[]>(url);
    }

    /**
     * Gets all users that meet the criteria
     * @param keyword a part of first or last name
     * @returns An Observable of keyvalue array.
     */
    getUsersByKeyword(keyword: string): Observable<any[]> {
        const url = environment.apiUrl + '/dictionary/users/' + keyword;
        return this.http.get<any[]>(url, { headers: { dontShowLoader: 'true' }});
    }
}
