import { Injectable } from '@angular/core';
import { PromptMessageState } from '../interfaces/prompt-message-state';
import { Subject } from 'rxjs';
import { PromptType } from '../enums/prompt-type';

@Injectable()
export class PromptService {

    private subject: Subject<PromptMessageState> = new Subject<PromptMessageState>();
    get promptState() { return this.subject.asObservable(); }

    /**
     * Shows information message.
     * @param title title
     * @param message information message
     */
    showInfoMessage(title: string, message: string) {
        this.subject.next({
            message,
            title,
            promptType: PromptType.Info
        } as PromptMessageState);
    }


    /**
     * Shows warning message.
     * @param title title
     * @param message warning message
     */
    showWarningMessage(title: string, message: string) {
        this.subject.next({
            message,
            title,
            promptType: PromptType.Warning
        } as PromptMessageState);
    }

    /**
     * Shows error message.
     * @param title title
     * @param message error message
     */
    showErrorMessage(title: string, message: string) {
        this.subject.next({
            message,
            title,
            promptType: PromptType.Error
        } as PromptMessageState);
    }

    /**
     * Shows custom confirm dialog.
     * @param title title
     * @param message question
     * @param yesAction callback after clicking Yes
     */
    showQuestion(title: string, message: string, yesAction: () => void) {
        this.subject.next({
            message,
            title,
            promptType: PromptType.Question,
            yesCallback: yesAction
        } as PromptMessageState);
    }
}
