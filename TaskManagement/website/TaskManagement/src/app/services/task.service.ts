import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatatableRequest } from '../models/datatable-request.model';
import { Observable } from 'rxjs';
import { DatatableResult } from '../models/datatable-result.model';
import { TaskGroup } from '../models/task-group.model';
import { environment } from 'src/environments/environment';
import { DatatableFilterInterface } from '../interfaces/datatable-filter.interface';
import { UserTask } from '../models/user-task.model';

@Injectable()
export class TaskService {
    constructor(private http: HttpClient) {
    }

    /**
     * Gets TaskGroup records that meet the criteria
     * @param request Datatable model - offset, limit, orders, columns etc.
     * @param filters custom filters
     * @returns An Observable of Datatable<TaskGroup> object
     */
    getTaskGroups(request: DatatableRequest, filters: DatatableFilterInterface): Observable<DatatableResult<TaskGroup>> {
        const url = environment.apiUrl + '/task-management/task-group?' + request.createParams(filters);
        return this.http.get<DatatableResult<TaskGroup>>(url);
    }

    /**
     * Gets TaskGroup record by iD
     * @param id record ID
     * @returns An Observable of TaskGroup object
     */
    getTaskGroupById(id: number): Observable<TaskGroup> {
        const url = environment.apiUrl + '/task-management/task-group/' + id;
        return this.http.get<TaskGroup>(url);
    }

    /**
     * Creates new TaskGroup
     * @param model - TaskGroup data
     * @returns An ID of newly created object
     */
    createTaskGroup(model: TaskGroup): Observable<number> {
        const url = environment.apiUrl + '/task-management/task-group';
        return this.http.post<number>(url, model);
    }

    /**
     * Updates the passed TaskGroup
     * @param model TaskGroup data
     * @returns Http response
     */
    updateTaskGroup(model: TaskGroup): Observable<any> {
        const url = environment.apiUrl + '/task-management/task-group';
        return this.http.put<any>(url, model);
    }

    /**
     * Removes TaskGroup by ID
     * @param id record ID
     * @returns Http response
     */
    removeTaskGroupById(id: number): Observable<any> {
        const url = environment.apiUrl + '/task-management/task-group/' + id;
        return this.http.delete<any>(url);
    }

    /**
     * Gets UserTask records that meet the criteria
     * @param request Datatable model - offset, limit, orders, columns etc.
     * @param filters custom filters
     * @returns An Observable of Datatable<UserTask> object
     */
    getUserTasks(request: DatatableRequest, filters: DatatableFilterInterface): Observable<DatatableResult<UserTask>> {
        const url = environment.apiUrl + '/task-management/user-task?' + request.createParams(filters);
        return this.http.get<DatatableResult<UserTask>>(url);
    }

    /**
     * Gets UserTask record by iD
     * @param id record ID
     * @returns An Observable of UserTask object
     */
    getUserTaskById(id: number): Observable<UserTask> {
        const url = environment.apiUrl + '/task-management/user-task/' + id;
        return this.http.get<UserTask>(url);
    }

    /**
     * Creates new UserTask
     * @param model - UserTask data
     * @returns An ID of newly created object
     */
    createUserTask(model: UserTask): Observable<number> {
        const url = environment.apiUrl + '/task-management/user-task';
        return this.http.post<number>(url, model);
    }

    /**
     * Updates the passed UserTask
     * @param model UserTask data
     * @returns Http response
     */
    updateUserTask(model: UserTask): Observable<any> {
        const url = environment.apiUrl + '/task-management/user-task';
        return this.http.put<any>(url, model);
    }

    /**
     * Removes UserTask by ID
     * @param id record ID
     * @returns Http response
     */
    removeUserTaskById(id: number): Observable<any> {
        const url = environment.apiUrl + '/task-management/user-task/' + id;
        return this.http.delete<any>(url);
    }
}
