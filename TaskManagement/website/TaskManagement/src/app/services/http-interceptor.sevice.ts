import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LoaderService } from './loader.service';
import { Injectable } from '@angular/core';
import { PromptService } from './prompt.service';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {

    constructor(
        private loaderService: LoaderService,
        private promptService: PromptService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        // checking if request contains 'dontShowLoader' header, if yes - loader ring is not displayed.
        const loaderHeader = request.headers.get('dontShowLoader');
        if (!loaderHeader) {
            this.showLoader();
        }

        request = request.clone({headers: request.headers.delete('dontShowLoader')});

        // displaying custom modal dialog with error message if error occured.
        return next.handle(request).pipe(
            tap((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    this.hideLoader();
                }
                return event;
            }, (error: HttpErrorResponse) => {
                this.promptService.showErrorMessage('Error', error.status + '-' + error.statusText);
                this.hideLoader();
            })
        );
    }

    private showLoader() {
        this.loaderService.showLoader();
    }

    private hideLoader() {
        this.loaderService.hideLoader();
    }
}
