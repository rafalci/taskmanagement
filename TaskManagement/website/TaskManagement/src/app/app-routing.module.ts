import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TaskGroupListComponent } from './component/task-group-list/task-group-list.component';
import { TaskGroupComponent } from './component/task-group/task-group.component';


const routes: Routes = [
  {path: '', component: TaskGroupListComponent},
  {path: 'task-group/:id', component: TaskGroupComponent},
  {path: 'task-group', component: TaskGroupComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
