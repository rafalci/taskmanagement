import { Component, OnInit, Input, ViewChild, Output, EventEmitter, OnDestroy, AfterViewInit } from '@angular/core';
import { UserTask } from 'src/app/models/user-task.model';
import { NgForm, NgModel } from '@angular/forms';
import { TaskService } from 'src/app/services/task.service';
import { Subscription, Subject } from 'rxjs';
import { DictionaryService } from 'src/app/services/dictionary.service';
import { MatAutocompleteSelectedEvent, MatAutocompleteTrigger } from '@angular/material';
import { distinctUntilChanged, debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-user-task',
  templateUrl: './user-task.component.html',
  styleUrls: ['./user-task.component.css']
})
export class UserTaskComponent implements OnInit, OnDestroy, AfterViewInit {
  @Input() taskStatusDictionary: any[];
  @Output() save = new EventEmitter();
  @Output() cancel = new EventEmitter();
  @ViewChild('userTaskForm', { static: false }) userTaskForm: NgForm;
  @ViewChild('user', { static: false, read: MatAutocompleteTrigger}) userAutocompleteTrigger: MatAutocompleteTrigger;

  userTask: UserTask = new UserTask(0);
  userSuggestions: any[];
  userSuggestionsAreLoading = false;
  userNameChanged: Subject<string> = new Subject<string>();
  userNameChangeSub: Subscription;
  userNameClosingAutocompleteSub: Subscription;

  constructor(
    private taskService: TaskService,
    private dictionaryService: DictionaryService) { }

  ngOnInit() {
    this.initializeAutocomplete();
  }

  ngAfterViewInit() {
    this.onClosingUserAutocomplete();
  }

  ngOnDestroy(): void {
    this.userNameChangeSub.unsubscribe();
  }

  /**
   * Allows or prevents editing form.
   * @param enable set to true to enable editing or false to disable editing
   */
  enableDisableForm(enable: boolean) {
    if (enable) {
      this.userTaskForm.form.enable();
    } else {
      this.userTaskForm.form.disable();
    }
  }

  /**
   * Loads UserTask data.
   * @param taskGroupId the parent ID
   * @param id optional - provide if user task exists
   */
  loadUserTask(taskGroupId: number, id?: number) {
    this.userTaskForm.reset();
    if (id) {
      this.taskService.getUserTaskById(id)
        .subscribe(result => {
          this.userTask = result;
        });
    } else {
      this.userTask = new UserTask(taskGroupId);
    }
  }

  submitUserTaskForm() {
    // creating or updating form based on userTaskId field.
    if (this.userTaskForm.valid) {
      if (!this.userTask.userTaskId) {
        this.taskService.createUserTask(this.userTask)
          .subscribe(id => {
            this.closeFormAfterSave();
          });
      } else {
        this.taskService.updateUserTask(this.userTask)
        .subscribe(result => {
          this.closeFormAfterSave();
        });
      }
    }
  }

  cancelForm() {
    this.cancel.emit();
  }

  userSelected(event: MatAutocompleteSelectedEvent) {
    this.userTask.userId = event.option.value.key;
    this.userTask.userName = event.option.value.value;
  }

  displayUserName(name) {
    // in some cases browser displays [Object object] instead of the proper name.
    if (typeof name !== 'undefined' && name !== null) {
      return typeof name === 'string'
        ? name
        : name.value;
    }
    return '';
  }

  private closeFormAfterSave() {
    // reseting validation after submitting the form.
    this.userTaskForm.resetForm();
    this.userTaskForm.form.markAsPristine();
    this.save.emit();
  }

  private initializeAutocomplete() {
    // initializing autocomplete for the user input.
    this.userNameChangeSub = this.userNameChanged.pipe(
      debounceTime(250),
      distinctUntilChanged()
    )
      .subscribe(value => {
        if (this.userTask.userName && this.userTask.userName.trim() !== '') {
          this.userSuggestionsAreLoading = true;
          this.dictionaryService.getUsersByKeyword(this.userTask.userName)
            .subscribe(result => {
              this.userSuggestions = result;
            }, err => {
              this.userSuggestionsAreLoading = false;
              this.userSuggestions = [];
            }, () => {
              this.userSuggestionsAreLoading = false;
            });
        } else {
          this.userSuggestions = [];
        }
      });
  }

  private onClosingUserAutocomplete() {
    // preventing from inserting any user name - it has to be selected from suggestions
    if (this.userNameClosingAutocompleteSub && !this.userNameClosingAutocompleteSub.closed) {
      this.userNameClosingAutocompleteSub.unsubscribe();
    }

    this.userNameClosingAutocompleteSub = this.userAutocompleteTrigger
      .panelClosingActions.subscribe(e => {
        if (!e || !e.source) {
          this.userTask.userId = null;
          this.userTask.userName = '';
          this.userSuggestions = [];
        }
      }, err => {
        this.onClosingUserAutocomplete();
      }, () => {
        this.onClosingUserAutocomplete();
      });
  }
}
