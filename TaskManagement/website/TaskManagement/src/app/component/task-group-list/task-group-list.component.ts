import { Component, OnInit, ViewChild } from '@angular/core';
import { RouterLinkInterface } from '../../interfaces/router-link.interface';
import { DatatableComponent } from '../common/datatable/datatable.component';
import { TaskGroup } from '../../models/task-group.model';
import { DatatableRequest } from '../../models/datatable-request.model';
import { DatatableActionInterface } from '../../interfaces/datatable-action.interface';
import { TaskService } from '../../services/task.service';
import { Router } from '@angular/router';
import { PromptService } from 'src/app/services/prompt.service';

@Component({
  selector: 'app-task-group-list',
  templateUrl: './task-group-list.component.html',
  styleUrls: ['./task-group-list.component.css']
})
export class TaskGroupListComponent implements OnInit {
  @ViewChild(DatatableComponent, {static: false}) dataTable: DatatableComponent<TaskGroup>;

  constructor(
    private taskService: TaskService,
    private router: Router,
    private promptService: PromptService) { }

  ngOnInit() {
  }

  /**
   * Links used by Breadcrumb container.
   */
  get links() {
    return [
      { title: 'Home', url: ''} as RouterLinkInterface,
      { title: 'Task Assigment', url: null} as RouterLinkInterface
    ];
  }

  /**
   * Grid action list.
   */
  get getActions(): DatatableActionInterface<TaskGroup> {
    const self = this;
    return {
      edit: self.editTaskGroup,
      remove: self.removeTaskGroup
    };
  }

  /**
   * Ajax call to get all task groups
   */
  getTaskGroups = (request: DatatableRequest) => {
    return this.taskService.getTaskGroups(request, null);
  }


  /**
   * Fires when clicking edit icon on the grid.
   */
  editTaskGroup = (request: TaskGroup) => {
    this.router.navigate(['/task-group', request.taskGroupId]);
  }

  /**
   * Fires when clicking delete icon on the grid.
   */
  removeTaskGroup = (request: TaskGroup) => {
    this.promptService.showQuestion('Comfirmation', 'Are you sure you want to delete this record?',
      () => {
        this.taskService.removeTaskGroupById(request.taskGroupId)
          .subscribe(result => {
            this.dataTable.reload();
          });
      });
  }

  createTaskGroup() {
    this.router.navigate(['/task-group']);
  }
}
