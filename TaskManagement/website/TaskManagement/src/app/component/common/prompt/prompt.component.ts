import { Component, AfterViewInit, OnDestroy, ViewChild, OnInit } from '@angular/core';
import { PromptMessageState } from 'src/app/interfaces/prompt-message-state';
import { Subscription } from 'rxjs';
import { PromptService } from 'src/app/services/prompt.service';
import { PromptType } from '../../../enums/prompt-type';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-prompt',
  templateUrl: './prompt.component.html',
  styleUrls: ['./prompt.component.css']
})
export class PromptComponent implements OnInit, OnDestroy {
  @ViewChild('autoShownModal', {static: false}) modal: ModalDirective;

  private subscription: Subscription;
  title: string;
  message: string;
  iconClass: any;
  promptType: PromptType;
  types = PromptType;
  yesCallback: () => void;

  constructor(private promptService: PromptService) { }

  ngOnInit(): void {
    this.subscription = this.promptService
      .promptState.subscribe(newState => {
        this.message = newState.message;
        this.title = newState.title;
        this.promptType = newState.promptType;
        this.yesCallback = newState.yesCallback;
        this.modal.show();
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  closeModal() {
    this.modal.hide();
  }

  /**
   * Fires when clicking Yes button on Question Prompt.
   */
  performAction() {
    this.modal.hide();
    this.yesCallback();
  }
}
