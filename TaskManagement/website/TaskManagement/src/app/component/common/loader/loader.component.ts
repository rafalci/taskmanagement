import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { LoaderService } from 'src/app/services/loader.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit, OnDestroy {


  private subscription: Subscription;
  show = false;

  constructor(private loaderService: LoaderService) { }

  ngOnInit() {
    this.subscription = this.loaderService
    .loaderState.subscribe(state => {
      this.show = state;
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
