import { Component, OnInit, ViewChild, Input } from '@angular/core';
import 'datatables.net';
import 'datatables.net-bs4';
import * as $ from 'jquery';
import { Observable } from 'rxjs';
import { DatatableActionInterface } from 'src/app/interfaces/datatable-action.interface';
import { DatatableRequest } from 'src/app/models/datatable-request.model';
import { DatatableResult } from 'src/app/models/datatable-result.model';

@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.css']
})
export class DatatableComponent<T> implements OnInit {

  @Input() callback: (request: DatatableRequest) => Observable<DatatableResult<T>>;
  @Input() actions: DatatableActionInterface<T>;

  private table: JQuery<HTMLElement>;
  private get dataTable() { return this.table.DataTable(); }
  private get dom() { return 'lrt<"bottom"pi><"clear">'; }
  private get language() {
    return {
      processing: `<div class="datatable-loader"><i class="fas fa-circle-notch fa-3x fa-spin"></i></div>`,
      lengthMenu: `<select class="form-control length-control">
        <option>10</option>
        <option>25</option>
        <option>50</option>
        <option>100</option>
      </select> items per page`
    };
  }

  constructor() { }

  ngOnInit() {
    this.table = $('table');

    if (this.table.length !== 1) {
      throw new Error('Datatable container must contain excatly one table!');
    }

    this.initializeDatatable();
  }

  reload() {
    this.dataTable.ajax.reload();
  }

  private initializeDatatable() {
    const self = this;

    this.table.DataTable({
      processing: false,
      serverSide: true,
      searching: false,
      orderMulti: false,
      lengthChange: false,
      scrollY: '500px',
      pageLength: 14,
      scrollCollapse: true,
      dom: self.dom,
      ajax: self.handleAjax,
      columns: self.handleColumns(),
      language: self.language
    });
  }

  private handleAjax = (data: DataTables.AjaxDataRequest, callback: ((data: any) => void)) => {
    const request = new DatatableRequest(data);

    this.callback(request).subscribe(result => {
      callback({
        data: result.data,
        recordsFiltered: result.recordsFiltered,
        recordsTotal: result.recordsTotal
      });
    });
  }

  private handleColumns = (): DataTables.ColumnSettings[] => {
    const self = this;

    this.table.on('click', 'button', function() {
      const button = $(this);
      const item = (self.dataTable.row(button.closest('tr')).data() as unknown) as T;
      const action = button.data('action');

      if (action) {
        self.actions[action](item);
      }
    });

    const columns = this.table.find('th').toArray();

    return columns.map(col => {
      const column = $(col);
      const columnType = column.data('type');

      switch (columnType) {
        case 'hidden':
          return {
            data: column.data('property'),
            visiblity: false,
            sClass: 'hide-column'
          };
        case 'text':
          return {
            data: column.data('property'),
          };
        case 'button':
          const action = column.data('action');
          const icon = column.data('icon');

          return {
            data: null,
            defaultContent:
              `<button type="button" class="btn btn-grid" data-action="${action}"><i class="${icon}"></i></button>`,
            width: '20px',
            sortable: false,
            searchable: false
          };
      }
    });
  }
}
