import { Component, OnInit, Input } from '@angular/core';
import { RouterLinkInterface } from 'src/app/interfaces/router-link.interface';

@Component({
  selector: 'app-breadcrumb-container',
  templateUrl: './breadcrumb-container.component.html',
  styleUrls: ['./breadcrumb-container.component.css']
})
export class BreadcrumbContainerComponent implements OnInit {

  @Input() links: RouterLinkInterface[];

  constructor() { }

  ngOnInit() {
  }

}
