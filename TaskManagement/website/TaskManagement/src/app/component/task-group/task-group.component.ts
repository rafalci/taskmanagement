import { Component, OnInit, ViewChild, AfterContentChecked } from '@angular/core';
import { RouterLinkInterface } from '../../interfaces/router-link.interface';
import { DatatableRequest } from '../../models/datatable-request.model';
import { TaskService } from '../../services/task.service';
import { DatatableActionInterface } from '../../interfaces/datatable-action.interface';
import { TaskGroup } from '../../models/task-group.model';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { UserTask } from 'src/app/models/user-task.model';
import { DictionaryService } from 'src/app/services/dictionary.service';
import { UserTaskComponent } from '../user-task/user-task.component';
import { DatatableFilterInterface } from 'src/app/interfaces/datatable-filter.interface';
import { PromptService } from 'src/app/services/prompt.service';
import { DatatableComponent } from '../common/datatable/datatable.component';

@Component({
  selector: 'app-task-group',
  templateUrl: './task-group.component.html',
  styleUrls: ['./task-group.component.css']
})
export class TaskGroupComponent implements OnInit {
  @ViewChild('taskGroupForm', {static: false}) taskGroupForm: NgForm;
  @ViewChild(UserTaskComponent, {static: false}) userTaskComponent: UserTaskComponent;
  @ViewChild(DatatableComponent, {static: false}) dataTable: DatatableComponent<TaskGroup>;

  taskGroup: TaskGroup = new TaskGroup();
  taskStatuses: any[];
  hideUserTaskForm = true;

  constructor(
    private taskService: TaskService,
    private route: ActivatedRoute,
    private router: Router,
    private dictionaryService: DictionaryService,
    private promptService: PromptService) {
  }

  ngOnInit() {
    this.loadDictionaries();
    this.loadTaskGroup();
  }

  get taskGroupId(): number {
    const taskgroupId = this.route.snapshot.paramMap.get('id');
    if (taskgroupId) {
      return +taskgroupId;
    }
    return 0;
  }

  /**
   * Links used by Breadcrumb container.
   */
  get links() {
    return [
      { title: 'Home', url: ''} as RouterLinkInterface,
      { title: 'Task Assigment', url: ''} as RouterLinkInterface,
      { title: 'Task Group', url: null} as RouterLinkInterface
    ];
  }

  /**
   * Grid action list.
   */
  get getActions(): DatatableActionInterface<UserTask> {
    const self = this;
    return {
      edit: self.editRequest,
      remove: self.removeRequest
    };
  }

  /**
   * Ajax call to get user tasks related to the following task group.
   */
  getUserTasks = (request: DatatableRequest) => {
    return this.taskService.getUserTasks(request, { taskGroupId: this.taskGroupId } as DatatableFilterInterface);
  }

  /**
   * Fires when clicking edit icon on the grid
   */
  editRequest = (request: UserTask) => {
    this.hideUserTaskForm = false;
    this.userTaskComponent.loadUserTask(this.taskGroupId, request.userTaskId);
  }

  /**
   * Fires when clicking delete icon on the grid
   */
  removeRequest = (request: UserTask) => {
    this.promptService.showQuestion('Comfirmation', 'Are you sure you want to delete this record?',
      () => {
        this.taskService.removeUserTaskById(request.userTaskId)
          .subscribe(result => {
            this.dataTable.reload();
          });
      });
  }

  backToList() {
    this.router.navigate(['']);
  }

  submitTaskGroupForm() {
    // creating or updating based on taskGroupId value.
    if (this.taskGroupForm.valid) {
      if (!this.taskGroup.taskGroupId) {
        this.taskService.createTaskGroup(this.taskGroup)
          .subscribe(id => {
            this.router.navigate(['/task-group', id]);
          });
      } else {
        this.taskService.updateTaskGroup(this.taskGroup)
        .subscribe(result => {
          this.taskGroupForm.form.markAsPristine();
        });
      }
    }
  }

  createUserTask() {
    // loading empty form for creating a new user task.
    this.hideUserTaskForm = false;
    this.userTaskComponent.loadUserTask(this.taskGroupId);
  }

  afterUserFormSave() {
    // hidding User Task form and reloading User Task table after saving changes
    this.hideUserTaskForm = true;
    this.dataTable.reload();
  }

  afterUserFormCancel() {
    // hidding User Task form after cancel changes
    this.hideUserTaskForm = true;
  }

  private loadTaskGroup() {
    if (this.taskGroupId !== 0) {
      this.taskService.getTaskGroupById(this.taskGroupId)
        .subscribe(result => {
          this.taskGroup = result;
        });
    }
  }

  private loadDictionaries() {
    // loading all static data like statuses
    this.dictionaryService.getTaskStatuses()
    .subscribe(result => {
      this.taskStatuses = result;
    });
  }

}
