import { Component, OnInit } from '@angular/core';
import { RouterLinkInterface } from 'src/app/interfaces/router-link.interface';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  get routerLinks() {
    return [
      { title: 'Task Assigment', url: ''} as RouterLinkInterface
    ];
  }
}
