import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { MatProgressSpinnerModule, MatInputModule, MatFormFieldModule, MatAutocompleteModule } from '@angular/material';
import { DragDropModule } from '@angular/cdk/drag-drop';


import { AppComponent } from './app.component';
import { HeaderComponent } from './component/master/header/header.component';
import { FooterComponent } from './component/master/footer/footer.component';
import { TaskGroupListComponent } from './component/task-group-list/task-group-list.component';
import { DatatableComponent } from './component/common/datatable/datatable.component';
import { BreadcrumbContainerComponent } from './component/common/breadcrumb-container/breadcrumb-container.component';
import { TaskService } from './services/task.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoaderComponent } from './component/common/loader/loader.component';
import { HttpInterceptorService } from './services/http-interceptor.sevice';
import { LoaderService } from './services/loader.service';
import { PromptComponent } from './component/common/prompt/prompt.component';
import { ModalModule } from 'ngx-bootstrap/modal/';
import { PromptService } from './services/prompt.service';
import { TaskGroupComponent } from './component/task-group/task-group.component';
import { FormsModule } from '@angular/forms';
import { DictionaryService } from './services/dictionary.service';
import { UserTaskComponent } from './component/user-task/user-task.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    TaskGroupListComponent,
    BreadcrumbContainerComponent,
    DatatableComponent,
    LoaderComponent,
    PromptComponent,
    TaskGroupComponent,
    UserTaskComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    ModalModule.forRoot(),
    MatInputModule,
    MatFormFieldModule,
    FormsModule,
    MatAutocompleteModule,
    DragDropModule
  ],
  providers: [
    TaskService,
    LoaderService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    },
    PromptService,
    DictionaryService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
