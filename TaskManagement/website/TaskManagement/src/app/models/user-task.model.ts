export class UserTask {

    constructor(taskGroupId: number) {
        this.taskGroupId = taskGroupId;
    }

    userTaskId: number;
    userTaskName: string;
    deadline: Date;
    userId: number;
    userName: string;
    statusId: number;
    statusName: string;
    taskGroupId: number;
}
