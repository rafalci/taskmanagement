export class TaskGroup {
    taskGroupId: number;
    taskGroupName: string;
    numberOfTasks: number;
}
