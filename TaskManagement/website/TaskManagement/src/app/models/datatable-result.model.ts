export class DatatableResult<T> {
    data: T[];
    recordsTotal: number;
    recordsFiltered: number;
}
