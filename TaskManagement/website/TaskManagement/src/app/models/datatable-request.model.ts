import { DatatableFilterInterface } from '../interfaces/datatable-filter.interface';
import * as $ from 'jquery';

export class DatatableRequest {
    start: number;
    length: number;
    order: DataTables.AjaxDataRequestOrder[];
    columns: DataTables.AjaxDataRequestColumn[];

    constructor(data: DataTables.AjaxDataRequest) {
        this.start = data.start;
        this.length = data.length;
        this.order = data.order;
        this.columns = data.columns;
    }

    createParams(filters: DatatableFilterInterface): string {
        return $.param({ model: this, filters });
    }
}
